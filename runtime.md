# public class ScriptRuntime

## 实例属性

 ### ScriptId - 当前脚本id

- string ScriptId { get; private set; }

### Ui - ui管理

-  UiManager Ui { get; private set; } 脚本需要通过它来创建界面

###  FloatyHelper - 悬浮窗相关

- FloatyHelper FloatyHelper { get; private set; } 脚本需要通过它来创建悬浮窗

### ThreadManager - 线程管理

- ThreadManager Threads { get; private set; } 脚本必须经过它来实现线程创建, 否则无法在脚本停止时卸载相关程序集

### Tasks - Task管理

- TaskManager Tasks { get; private set; } 脚本必须经过此api来实现task创建, 否则无法在脚本停止时卸载相关程序集

### PermissionHelper - 权限相关

- PermissionHelper PermissionHelper { get; private set; } 

### IsUiMode - 是否为ui模式

- bool IsUiMode { get; private set; }

### WorkDir - 脚本工作路径

- string WorkDir { get; private set; }

### Activity - 脚本自身的activity

- TemplateActivity Activity { get; private set; }

非ui模式时为null

### IsExitAppOnStoped - 是否在脚本停止时退出应用

-  bool IsExitAppOnStoped { get; set; }

只在打包apk有效



## 实例方法

### SetStop - 停止脚本

- void SetStop()

```c#
Runtime.SetStop();
```



### AddExitCallback - 添加一个脚本停止时的回调

- void AddExitCallback(Action callback)

```c#
Runtime.SetStop(()=>
{
    Globals.Toast("脚本退出");
});
```



### AddLoggerCallback - 添加一个logger的回调

- string AddLoggerCallback(`Action<LogLevel, DateTime, string>` callback)
- `LogLevel` 日志级别
- `DateTime` 日志时间
- `string` 日志内容

```c#
var logKey = Runtime.AddLoggerCallback((level, time, msg)=>
{
	Globals.Toast($"日志回调: {msg}");
});
```



### RemoveLoggerCallback - 移除logger的回调

- void RemoveLoggerCallback(string key = null)
- `key`  回调的key, 当key为空时移除当前runtime添加的所有回调

```c#
Runtime.RemoveLoggerCallback(logKey);
```

