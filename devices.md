# Devices - 设备参数

## 静态属性

### Dp - 屏幕密度

### Dpi - 以每英寸点数表示的屏幕密度。

### Width - 屏幕宽度

### Height - 屏幕高度

### RealWidth - 与旋转方向无关的屏幕宽度

### ReadHeight - 与旋转方向无关的屏幕高度

### Id - 修订版本号

### Board - 主板

### Brand - 系统定制商

### SupportedAbis - CPU指令集

### Device - 设备参数

### Display - 显示屏参数

### Fingerprint - 唯一编号

### Serial - 硬件序列号

> 此方法在android 8.0以上支持



### Manufacturer - 硬件制造商

### Model - 版本

### Hardware - 硬件名

### Product - 手机产品名

### Tags - 描述Build的标签

### Type - Builder类型

### Codename - 当前开发代码

### Incremental - 源码控制版本号

### Release - 版本字符串

### SdkInt - SDK版本号

### Host - Host值

### User - User名

### Time - 编译时间

### AndroidId

一个64位的数字(以十六进制字符串表示)，对应用程序签署密钥、用户和设备的每个组合来说都是独一无二的