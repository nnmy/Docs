# Automator - 自动化操作

此类依赖于无障碍服务, 请确保无障碍服务已开启

## 静态方法

### Click - 点击坐标

- static void Click(int x, int y, int duration = 1)
- `duration` 持续时间, 单位为毫秒, 默认1ms

```c#
Automator.Click(100, 100);
```



### Click - 点击坐标(给定范围的中心点)

- static void Click(Rect bounds, int duration = 1)
- `bounds` 范围
- `duration` 持续时间, 单位为毫秒, 默认1ms

```c#
var bounds = new astator.Core.Graphics.Rect(100,100,200,200);
Automator.Click(bounds);
```



### Swipe - 滑动

- static void Swipe(int startX, int startY, int endX, int endY, int duration)
- `duration` 持续时间, 单位为毫秒

```c#
Automator.Swipe(100, 100, 500, 100, 1000);
```



### GetWindows - 获取所有窗口根节点

- static `List<AccessibilityNodeInfo>` GetWindowRoots()

```C#
var windows = Automator.GetWindows();
```



### GetWindowRoot - 获取指定包名的窗口根节点

- static AccessibilityNodeInfo GetWindowRoot(string pkgName)
- `pkgName` 包名

```C#
var window = Automator.GetWindowRoot("com.astator.astator");
```



### GetCurrentWindowRoot - 获取当前活动应用的窗口根节点

- static AccessibilityNodeInfo GetCurrentWindowRoot()

```c#
var window = Automator.GetCurrentWindowRoot();
```



### GetCurrentPackageName - 获取当前活动应用包名

- static string GetCurrentPackageName()

```c#
var pkgName = Automator.GetCurrentPackageName();
```



## 节点操作 - 节点属性

### PackageName - 包名

```c#
var node = Automator.GetCurrentWindowInfo();
var pkgName = node.PackageName;
```



### ClassName - 类名

```c#
var clsName = node.ClassName;
```



### Text - 文本

```c#
var text = node.Text;
```



### Description - 内容描述

```c#
var desc = node.Description;
```



### Checkable - 是否可检查

```c#
var checkable = node.Checkable;
```



### Clickable - 是否可点击

```c#
var clickable = node.Clickable;
```



### Editable - 是否可编辑

```c#
var editable = node.Editable;
```



### Enabled - 是否已启用

```c#
var enabled = node.Enabled;
```



### Focusable - 是否可设置为焦点

```c#
var focusable = node.Focusable;
```



### DrawingOrder - 与此节点对应的视图的绘制顺序

```c#
var drawingOrder = node.DrawingOrder;
```



## 节点操作 - 扩展方法

### Find - 查找符合条件的所有节点

- static List<Accessibilitynode> Find(this Accessibilitynode node, SearcherArgs args)

```c#
var node = Automator.GetCurrentWindowInfo();
var result = node.Find(new SearcherArgs
{
   Text = "关于" 
});
```



### FindOne - 查找符合条件的第一个节点

- static Accessibilitynode FindOne(this Accessibilitynode node, SearcherArgs args)

```c#
var node = Automator.GetCurrentWindowInfo();
var result = node.FindOne(new SearcherArgs
{
   Text = "关于"
});
```



### GetId - 获取控件id

-  static string GetId(this Accessibilitynode node)

```c#
var id = node.GetId();
```



### GetBounds - 获取控件所在范围

- static Rect GetBounds(this Accessibilitynode node)

```c#
var bounds = node.GetBounds();
```



### GetDepth - 获取控件相对于根节点的深度

- static int GetDepth(this Accessibilitynode node)

```c#
var depth = node.GetDepth();
```



### Click - 点击控件

- static bool Click(this Accessibilitynode node)

```c#
var result = node.Click();
```



### LongClick - 长按控件

- static bool LongClick(this Accessibilitynode node)

```c#
var result = node.LongClick();
```



### SetText - 设置编辑框文本

- static bool SetText(this Accessibilitynode node, string text)

```c#
var result = node.SetText("test");
```



### SetSelectionText- 选中编辑框内容

- static bool SetSelectionText(this Accessibilitynode node, int start, int end)

```c#
var result = node.SetSelectionText(0, 2);
```



### ScrollForward - 向前滚动控件内容

- static bool ScrollForward(this Accessibilitynode node)

```c#
var result = node.ScrollForward();
```



### ScrollBackward - 向后滚动控件内容

- static bool ScrollBackward(this Accessibilitynode node)

```c#
var result = node.ScrollForward();
```

