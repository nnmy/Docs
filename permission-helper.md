# PermissionHelper - 权限相关

## 实例方法

### StartActivity - 启动意图

- void StartActivity(Intent intent)

```c#
var intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("https://astator.gitee.io/docs"));
Runtime.PermissionHelper.StartActivity(intent);
```



### StartActivityForResult - 启动意图并添加回调

- void StartActivityForResult(Intent intent, `Action<ActivityResult>` callback)

```c#
var manager = (MediaProjectionManager)Runtime.Activity.GetSystemService("media_projection");
var intent = manager.CreateScreenCaptureIntent();
Runtime.PermissionHelper.StartActivityForResult(intent, (result) =>
{
    if (result.ResultCode == (int)Result.Ok) Globals.Toast("申请截图权限成功!");
    else Globals.Toast("申请截图权限失败!");
});
```



### CheckScreenCap - 检查截图服务是否开启

- bool CheckScreenCap()

```c#
var result = Runtime.PermissionHelper.CheckScreenCap();
```



### ReqScreenCap - 申请截图权限及服务

- void ReqScreenCap(bool isLandscape, `Action<bool>` callback)
- `isLandscape`  是否为横屏

```c#
Runtime.PermissionHelper.ReqScreenCap(true, (result)=>
{
  if (result) Globals.Toast("申请截图权限成功!");
    else Globals.Toast("申请截图权限失败!");
});
```



### ReqScreenCapAsync - 申请截图权限及服务并返回结果

- async `Task<bool>` ReqScreenCapAsync(bool isLandscape)
- `isLandscape`  是否为横屏

```c#
var result = await Runtime.PermissionHelper.ReqScreenCapAsync(true);
```



### CloseScreenCap - 关闭截图服务

- void CloseScreenCap()

```c#
Runtime.PermissionHelper.CloseScreenCap();
```



### CheckFloaty - 检查悬浮窗权限

- bool CheckFloaty()

```c#
var result = Runtime.PermissionHelper.CheckFloaty();
```



### ReqFloaty - 申请悬浮窗权限

- void ReqFloaty(`Action<bool>` callback)

```
Runtime.PermissionHelper.ReqFloaty((isEnabled)=>
{
	if (result) Globals.Toast("申请悬浮窗权限成功!");
    else Globals.Toast("申请悬浮窗权限失败!");
});
```



### ReqFloatyAsync - 申请悬浮窗权限并返回结果

- async `Task<bool>` ReqFloatyAsync()

```c#
var result = await Runtime.PermissionHelper.ReqFloatyAsync();
```



### CheckAccessibility - 检查无障碍服务是否开启

- bool CheckAccessibility()

```
var result = Runtime.PermissionHelper.CheckAccessibility();
```



### ReqAccessibility - 启动无障碍服务

- void ReqAccessibility()

```c#
Runtime.PermissionHelper.ReqAccessibility((isEnabled)=>
{
	if (result) Globals.Toast("无障碍服务已开启!");
    else Globals.Toast("无障碍服务未启动!");
});
```



### ReqAccessibilityAsync - 启动无障碍服务并返回结果

- async `Task<bool>` ReqAccessibilityAsync()

```C#
var result = await Runtime.PermissionHelper.ReqAccessibilityAsync();
```



### CloseAccessibility - 关闭无障碍服务

- void CloseAccessibility()

```c#
Runtime.PermissionHelper.CloseAccessibility();
```



### CheckIgnoringBatteryOptimizations - 检查忽略电池优化是否开启

- bool IsIgnoringBatteryOptimizations()

```c#
var result = Runtime.PermissionHelper.IsIgnoringBatteryOptimizations();
```



### ReqIgnoringBatteryOptimizations - 申请忽略电池优化

- void ReqIgnoringBatteryOptimizations()

```c#
Runtime.PermissionHelper.IgnoringBatteryOptimizations((isEnabled)=>
{
	if (result) Globals.Toast("申请悬浮窗权限成功!");
    else Globals.Toast("申请悬浮窗权限失败!");
});
```



### ReqIgnoringBatteryOptimizationsAsync - 申请忽略电池优化并返回结果

- async `Task<bool>` ReqIgnoringBatteryOptimizationsAsync()

```c#
var result = await Runtime.PermissionHelper.ReqIgnoringBatteryOptimizationsAsync();
```



### CheckUsageStats - 检查使用情况统计权限是否开启

- bool CheckUsageStats()

```c#
var result = Runtime.PermissionHelper.CheckUsageStats();
```



### ReqUsageStats - 申请使用情况统计权限

- void ReqUsageStats()

```c#
Runtime.PermissionHelper.ReqUsageStats((isEnabled)=>
{
	if (result) Globals.Toast("申请使用情况统计权限成功!");
    else Globals.Toast("申请使用情况统计权限失败!");
});
```



### ReqUsageStatsAsync - 申请使用情况统计权限并返回结果

- async `Task<bool>` ReqUsageStatsAsync()

```c#
var result = await Runtime.PermissionHelper.ReqUsageStatsAsync();
```



### CheckPermission - 检查权限是否开启

- bool CheckPermission(string permission)
- `permission`  权限字符串, 详情查阅https://developer.android.google.cn/reference/android/app/AppOpsManager?hl=en#constants_1

```c#
if(CheckPermission("android:get_usage_stats"))
{
    Globals.Toast("使用情况统计权限已开启");
}   
```



### ReqPermission - 申请动态权限

- void ReqPermission(string permission, Action<bool> callback)

```c#
var permission = "android.permission.READ_EXTERNAL_STORAGE";
Runtime.PermissionHelper.ReqPermission(permission, (result)=>
{
    if (result) Globals.Toast("申请动态权限成功!");
    else Globals.Toast("申请动态权限失败!");
});
```



### ReqPermissionAsync - 申请动态权限并返回结果

- async `Task<bool>` ReqPermissionAsync(string permission)

```c#
var permission = "android.permission.READ_EXTERNAL_STORAGE";
var result = await Runtime.PermissionHelper.ReqPermissionAsync(permission);
```

