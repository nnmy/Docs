# Preferences - 简单数据储存

用于储存简单的数据类型, 支持`string`, `bool`, `int`, `double`, `float`, `long`, `DateTime`

## 静态方法

###  Get - 获取数据

- static T Get`<T>`(string key, T defaultValue, string sharedName = null)
- `T` 泛型
- `key` 键
- `defaultValue` 默认值, 当key不存在时返回
- `sharedName` 共享名称, 为null时采用默认名称
- `TypeNotSupportedException` 当类型不支持时抛出异常

```c#
var i32 = Preferences.Get<int>("i32Test", -1);
var str = Preferences.Get<string>("strTest", "error", "test");
```



### Set - 设置数据

- static void Set(string key, object value, string sharedName = null)
- `key` 键
- `value` 要设置的值
- `TypeNotSupportedException` 当类型不支持时抛出异常

```c#
Preferences.Set("i32Test",1024);
Preferences.Set("strTest","astator","test");
```



### ContainsKey - 判断key是否存在

- static bool ContainsKey(string key, string sharedName = null)

```c#
var result = Preferences.ContainsKey("i32Test");
var result = Preferences.ContainsKey("strTest", "test");
```



### Remove - 移除一个key

- static void Remove(string key, string sharedName = null)

```c#
Preferences.Remove("i32Test");
Preferences.Remove("strTest", "test");
```



### Clear - 清除所有数据

- static void Clear(string sharedName = null)

```c#
Preferences.Clear();
Preferences.Clear("test);
```



## 实例方法

### Preferences - 构造方法

- Preferences(string sharedName)
- `sharedName` 共享名称

创建指定共享名称的对象

```c#
var settings = new Preferences("settings");
```



###  Get - 获取数据

- T Get`<T>`(string key, T defaultValue)
- `T` 泛型
- `key` 键
- `defaultValue` 默认值, 当key不存在时返回
- `TypeNotSupportedException` 当类型不支持时抛出异常

```c#
var i32 = settings.Get<int>("i32Test", -1);
var str = settings.Get<string>("strTest", "error");
```



### Set - 设置数据

- void Set(string key, object value, string sharedName = null)
- `key` 键
- `value` 要设置的值
- `TypeNotSupportedException` 当类型不支持时抛出异常

```c#
settings.Set("i32Test",1024);
settings.Set("strTest","astator");
```



### ContainsKey - 判断key是否存在

- bool ContainsKey(string key)

```c#
var result = settings.ContainsKey("i32Test");
var result = settings.ContainsKey("strTest");
```



### Remove - 移除一个key

- void Remove(string key)

```c#
settings.Remove("i32Test");
settings.Remove("strTest");
```



### Clear - 清除所有数据

- void Clear()

```c#
settings.Clear();
```