

## 简述<!-- {docsify-ignore-all} -->

> astator使用`C#`作为脚本语言, 基于`.net6`, 支持`c#10`语法, 因此你需要掌握`c#`的基本语法和数据类型。



## 优势

- 编译运行, 没有性能损失
- 支持`nuget`生态
- 可调用`.net6` api
- 可调用`android` api


## api

当前处于内测开发阶段, 所有api都是不稳定的。



## 演示视频

<div class="iframe-container">
<iframe src="http://player.bilibili.com/player.html?aid=339643818&bvid=BV1KR4y1V7hH&cid=546083754&page=1&high_quality=1&danmaku=0" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>
</div>
