# UI - 界面

注意: 在astator中, 所有控件的大小单位都是dp



## 主题 - DefaultTheme

### LayoutBackgroundColor - 布局背景色

- static Color LayoutBackground { get; set; } = Color.Transparent

### ColorPrimary - 主要颜色

- static Color ColorPrimary { get; set; } = Color.ParseColor("#2b0b98");

### ColorHint - 提示颜色

- static Color ColorHint { get; set; } = Color.ParseColor("#80808080");

### TextColor - 文本颜色

- static Color TextColor { get; set; } = Color.ParseColor("#4a4a4d");

### TextSize - 文本大小

- static int TextSize { get; set; } = 12;

修改主题可更改所有控件的呈现, 必须在ui创建前修改才有效, 示例:

```
DefaultTheme.ColorPrimary = Color.ParseColor("#0000ff");
DefaultTheme.TextSize = 15;
DefaultTheme.TextColor = Color.ParseColor("#ff0000");
```



## 视图 - Views

### 实例属性(通用)

#### CustomId - 自定义id

- string CustomId { get; set; }

仅供ui索引器使用



### 实例方法(通用)

#### GetAttr - 获取控件属性

- object GetAttr(string key);
- `key` 属性名
- `return`  key对应的值
- `AttributeNotExistException` 当key不存在时抛出异常



#### SetAttr - 设置控件属性

- void SetAttr(string key, object value);
- `key` 属性名
- `value` 要设置的值
- `AttributeNotExistException` 当key不存在时抛出异常



#### On - 添加监听器

- void On(string key, object listener);
- `key` 监听器的key
-  `listener` 监听器实例
-  `AttributeNotExistException` 当key不存在时抛出异常



示例:

```c#
var btn = Runtime.Ui.Create("btn", new ViewArgs
{
    ["id"] = "test",
    ["text"] = "点我"
});
btn.On("click", new OnClickListener((v) =>
{
	Console.WriteLine(btn.CustomId);
    var text = Runtime.Ui["test"].GetAttr("text");//使用ui索引器
	btn.SetAttr("text", $"{text}!");
}));
```



### 控件 - Controls

待完善...

### 布局 - Layouts

待完善...
