# GraphicHelper - 图色助手类

## 静态方法

### Create - 创建对象

- static GraphicHelper Create()

当初始化失败时返回null

```c#
var helper = GraphicHelper.Create();
```



## 实例方法

<!--### ReInitialize - 重新初始化-->

### KeepScreen - 获取截图数据

- bool KeepScreen(bool sign)
- `sign` 是否需要调用多点找色或找图

```c#
if (helper.KeepScreen())
{
    Globals.Toast("获取截图数据成功");
}
```



### UpdateRedList - 更新r值映射集合, 用于多点找色和找图

- void UpdateRedList()

```c#
helper.UpdateRedList();
```



### GetImage - 获取WrapImage对象

- WrapImage GetImage()
- WrapImage GetImage(Rect bounds)
- WrapImage GetImage(int left, int top, int right, int bottom)
- `bounds` 范围
- `left` `top` `right` `bottom`  左 上 右 下

> 当范围为空时返回全图

```c#
var image_1 = helper.GetImage();
var image_2 = helper.GetImage(new Rect(0, 0, 100, 100));
var image_3 = helper.GetImage(0, 0, 100, 100);
```



### GetPixel - 获取指定像素数据

- `int[]` GetPixel(int x, int y)

- string GetPixelStr(int x, int y)
- int GetPixelHex(int x, int y)

```c#
var rgb = helper.GetPixel(100,100);
var hexStr = helper.GetPixelStr(100,100);
var hexInt = helper.GetPixelHex(100,100);
```



### ParseCmpColorString - 解析多点比色色组数据

- `int[][]` ParseCmpColorString(string str)
- `str` 色组字符串

```c#
var data = helper.ParseCmpColorString("176|149|0x263238,110|340|0x37474f,106|514|0xfafafa");
```



### ParseFindColorString - 解析多点找色色组数据

- `int[][]` ParseFindColorString(string str)
- `str` 色组字符串

```c#
var data = helper.ParseFindColorString("176|149|0x263238,110|340|0x37474f,106|514|0xfafafa");
```



### GetCmpColorData - 获取单点比色色组数据

- short[] GetCmpColorData(int x, int y, int color)
- `color` 颜色值

```c#
var data = helper.GetCmpColorData(100, 100, 0xffffff);
```



### CompareColor - 单点比色

- bool CompareColor(short[] data, int sim, bool isOffset)
- `data` 色组数据
- `sim` 相似度, 范围0~100
- `isOffset` 是否偏移查找

```C#
var result = helper.CompareColor(data, 95, true);
```



### CompareMultiColor - 多点比色

- bool CompareMultiColor(`int[][]` data, int sim, bool isOffset)
- `data` 色组数据
- `sim` 相似度, 范围0~100
- `isOffset` 是否偏移查找

```c#
var result = helper.CompareMultiColor(desc, 95, true);
```



### CompareMultiColorLoop - 条件循环多点比色

- bool CompareMultiColorLoop(`int[][]` data, int sim, bool isOffset, int timeout, int timelag, bool sign)
- `timeout` 超时时间, 单位为毫秒
- `timelag` 间隔时间, 单位为毫秒
- `sign` 跳出条件, true为比色成功时返回, false为比色失败时返回

```c#
var result = helper.CompareMultiColorLoop(desc, 95, true, 5000, 500, true)
```



### FindMultiColor - 多点找色

- Point FindMultiColor(int left, int top, int right, int bottom, `int[][]` data, int sim, bool isOffset)
- Point FindMultiColor(Rect bounds, `int[][]` data, int sim, bool isOffset)
- `left` 查找范围: 左上 x 坐标
- `top` 查找范围: 左上 y 坐标
- `right` 查找范围: 右下 x 坐标
- `bottom` 查找范围: 右下 y 坐标
- `bounds` 查找范围
- `data` 色组数据
- `sim` 相似度, 范围0~100
- `sim` 相似度, 0~100
- `isOffset` 是否偏移查找

```c#
var point = helper.FindMultiColor(0, 0, 1000, 1000, desc, 95, true);
var bounds = new Rect(0, 0, 1000, 1000);
var point = helper.FindMultiColor(bounds, desc, 95, true);
```



### FindMultiColorEx - 多点找色并返回所有坐标

- `List<Point>` FindMultiColorEx(int left, int top, int right, int bottom, `int[][]` data, int sim, int filterNum = -1)
- `List<Point>` FindMultiColorEx(Rect bounds, `int[][]` data, int sim, int filterNum = -1)
- `filterNum` 过滤半径, 默认-1, 去除重叠区域

```
var points = helper.FindMultiColorEx(0, 0, 1000, 1000, desc, 95, -1);
var bounds = new Rect(0, 0, 1000, 1000);
var points = helper.FindMultiColorEx(bounds, desc, 95, -1);
```



### FindMultiColorLoop - 条件循环多点找色

- Point FindMultiColorLoop(int left, int top, int right, int bottom, `int[][]` data, int sim, bool isOffset, int timeout, int timelag, bool sign)
- Point FindMultiColorLoop(Rect bounds, `int[][]` data, int sim, bool isOffset, int timeout, int timelag, bool sign)
- `timeout` 超时时间, 单位为毫秒
- `timelag` 间隔时间, 单位为毫秒
- `sign` 跳出条件, true为找色成功时返回, false为找色失败时返回

```c#
var point = helper.FindMultiColorLoop(0, 0, 1000, 1000, desc, 95, true, 5000, 500, true);
var bounds = new Rect(0, 0, 1000, 1000);
var point = helper.FindMultiColor(bounds, desc, 95, true, 5000, 500, true);
```



### FindImage - 找图

- Point FindImage(Rect bounds, WrapImage image, int sim)

- Point FindImage(int left, int top, int right, int bottom, WrapImage image, int sim)
- `image` 目标图像

```c#
var image = WrapImage.CreateFromFile(Path.Combine(runtime.WorkDir, "assets", "findImage.png"));
var point = helper.FindImage(0, 0, 1000, 1000, image, 95);
```



### FindImageEx - 找图并返回所有坐标

- `List<Point>` FindImageEx(Rect bounds, WrapImage image, int sim)
- `List<Point>` FindImageEx(int left, int top, int right, int bottom, WrapImage image, int sim, int filterNum = -1)

- `filterNum` 过滤半径, 默认-1, 去除重叠区域

```c#
var image = WrapImage.CreateFromFile(Path.Combine(runtime.WorkDir, "assets", "findImage.png"));
var points = helper.FindImageEx(0, 0, 1000, 1000, image, 95);
```



### FindImageLoop - 条件循环找图

- Point FindImageLoop(Rect bounds, WrapImage image, int sim, int timeout, int timelag, bool sign)

- Point FindImageLoop(int left, int top, int right, int bottom, WrapImage image, int sim, int timeout, int timelag, bool sign)
- `timeout` 超时时间, 单位为毫秒
- `timelag` 间隔时间, 单位为毫秒
- `sign` 跳出条件, true为找色成功时返回, false为找色失败时返回

```c#
var image = WrapImage.CreateFromFile(Path.Combine(runtime.WorkDir, "assets", "findImage.png"));
var point = helper.FindImageLoop(0, 0, 1000, 1000, image, 95, 5000, 500, true);
```



# WrapImage - 图像包装类

##  实例属性

### Width - 图像宽度

### Height - 图像高度

### RowStride - 一行所占的字节数

### PxFormat - 像素通道数

### Data - 像素数据



## 静态方法

### CreateFromFile - 从文件路径创建WrapImage对象

- static WrapImage CreateFromFile(string path)

```c#
var image = WrapImage.CreateFromFile(Path.Combine(runtime.WorkDir, "assets", "findImage.png"));
```



## 实例方法

### GetFindImageData - 获取找图色组数据

- short[][] GetFindImageData()

```C#
var data = image.GetFindImageData();
```



### GetBitmap - 获取bitmap对象

- Bitmap GetBitmap()

> 注意: bitmap对象需要自己手动释放: `bitmap.Recycle();`

```c#
var bitmap = image.GetBitmap();
bitmap.Recycle();
```



### SaveToPng - 以png格式保存到文件

- void SaveToPng(string path)

```c#
image.SaveToPng(Path.Combine(runtime.WorkDir, "assets", "SaveToPng.png"));
```

